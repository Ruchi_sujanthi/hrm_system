-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2020 at 06:19 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrm_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_05_07_020812_create_permissions_table', 2),
(9, '2014_10_12_100000_create_password_resets_table', 3),
(10, '2020_02_18_083013_create_cm_msg_table', 3),
(11, '2020_02_18_120601_create_questionnaires', 3),
(12, '2020_02_19_110608_create_plans', 3),
(13, '2020_02_20_054636_create_jobs_table', 3),
(14, '2020_03_31_054840_create_survey_details', 3),
(15, '2020_03_31_094213_create_client_preferences', 3),
(16, '2020_03_31_094539_create_specialities', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0e5625cc8781c4d57552d42d0f9e418dae2b9bda62d713cfb9ab709c1284ee7b1e1ddbd2c2678d26', 1, 1, 'hrm_system', '[]', 0, '2020-05-07 01:05:52', '2020-05-07 01:05:52', '2021-05-07 06:35:52'),
('1394dcbd8a5b4a0cafaf40c1b566662f8ee3769d6deb1073a51176e78a7db4a426a6295049d5ec87', 1, 1, 'customer_desk', '[]', 0, '2020-05-07 22:19:18', '2020-05-07 22:19:18', '2021-05-08 03:49:18'),
('14f129d0ce90ac8ab9f962b2bafa633a19c5d16411780b9bb4ed5499016893f7368d096f3bbad06e', 1, 1, 'hrm_system', '[]', 0, '2020-05-06 22:53:01', '2020-05-06 22:53:01', '2021-05-07 04:23:01'),
('1d9cc559407d3501d54f2c812d77eaab601d33740766ac91f8051939412cd3094d943820fd2e0864', 1, 1, 'hrm_system', '[]', 0, '2020-05-07 01:13:50', '2020-05-07 01:13:50', '2021-05-07 06:43:50'),
('2c195290b9d578d5b0d79d1ccaf0713364f6c5ee183d6e0d278774e7634eced7b950ceddaa90a489', 1, 1, 'hrm_system', '[]', 0, '2020-05-06 21:53:10', '2020-05-06 21:53:10', '2021-05-07 03:23:10'),
('2d7c6663dea1746acbdf8e69900193b60504d217de78f9b83f41158829d9928b357cc562a0583955', 1, 1, 'hrm_system', '[]', 0, '2020-05-06 21:56:38', '2020-05-06 21:56:38', '2021-05-07 03:26:38'),
('36cafd7677d9521fd50ac5120c1d61b5ae3dd532c98074fbf88bbd2b1a5d1f3757c6b7ffca3ead87', 1, 1, 'hrm_system', '[]', 0, '2020-05-06 22:40:08', '2020-05-06 22:40:08', '2021-05-07 04:10:08'),
('4dff2502f07c503ecfa5fd0db874681b7e8bc10c6f8d520bffae5a227f18591f0b26a6992fc5d9e3', 1, 1, 'hrm_system', '[]', 0, '2020-05-06 22:55:29', '2020-05-06 22:55:29', '2021-05-07 04:25:29'),
('8f7dc71c0c379deddbd3bfad2cd97c88811bda8fa66d78dc1f85c5024b676cf8c46c64374af3f87c', 1, 1, 'hrm_system', '[]', 0, '2020-05-05 22:03:19', '2020-05-05 22:03:19', '2021-05-06 03:33:19'),
('98a9168cdcf13d5db3b876d1eb1e666e51d12cb7558ee884561d210010180b73caeae48aeb351d2c', 1, 1, 'hrm_system', '[]', 0, '2020-05-06 09:56:46', '2020-05-06 09:56:46', '2021-05-06 15:26:46'),
('9c2bd56e19c90a91361b59386fe15868b1f435e47cf13a7a77cd4b0d0f1a8e12cf2e7c9898019fa1', 1, 1, 'hrm_system', '[]', 0, '2020-05-07 01:30:09', '2020-05-07 01:30:09', '2021-05-07 07:00:09'),
('9f39cf4e6c7e3dd659714bf1c42c614ae5c40d0cef900ee5d3bf0402c42fa9a84d10d5dba8eed299', 1, 1, 'hrm_system', '[]', 0, '2020-05-07 01:05:40', '2020-05-07 01:05:40', '2021-05-07 06:35:40'),
('aff2537bfe641d6b2dd79d7ec27388aa1c8fe8e2a4495b09ffee6bcdfaf414ac14568d67b8feb2c5', 1, 1, 'hrm_system', '[]', 0, '2020-05-05 22:17:37', '2020-05-05 22:17:37', '2021-05-06 03:47:37'),
('b780ccadaaf308595c05e07e1f61d3e4fda97fe7e1990606ff05f3cbed43899ef9579f30c604ae06', 1, 1, 'hrm_system', '[]', 0, '2020-05-07 20:50:16', '2020-05-07 20:50:16', '2021-05-08 02:20:16'),
('c759d13948076318c3afd1509d398554e92b6d9524e9171c0eb94616e42827a323564a2f0518c1ab', 1, 1, 'hrm_system', '[]', 0, '2020-05-07 22:14:09', '2020-05-07 22:14:09', '2021-05-08 03:44:09'),
('d45e0a67fc14a2dd05e7d48a94427466ebf5d3057c2860583a6a3e4da6dc2b874041a24cbbf3bdee', 1, 1, 'hrm_system', '[]', 0, '2020-05-06 09:54:24', '2020-05-06 09:54:24', '2021-05-06 15:24:24'),
('d8db3491ce200ced039a7845e6367dfe14f1ce3e884661139039e827289a538bc248a0ba63f10027', 1, 1, 'hrm_system', '[]', 0, '2020-05-06 22:54:56', '2020-05-06 22:54:56', '2021-05-07 04:24:56'),
('f0278b71215242206a8aa6b3853cb8fdfee9d99a6fe779affee4b262ac79de0095008399c68dd1d4', 1, 1, 'hrm_system', '[]', 0, '2020-05-07 11:22:22', '2020-05-07 11:22:22', '2021-05-07 16:52:22');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'hrm Personal Access Client', '3XiG0YuUHLMaLIHPjebXBNeWm4ET50wgg46KvCxq', 'http://localhost', 1, 0, 0, '2020-05-05 07:03:27', '2020-05-05 07:03:27'),
(2, NULL, 'hrm Password Grant Client', '4kAbKqYLys1FA2reG85tkqX5pNp59tcg5gjpyfQ2', 'http://localhost', 0, 1, 0, '2020-05-05 07:03:27', '2020-05-05 07:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-05 07:03:27', '2020-05-05 07:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `slug` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `emp_id` int(11) NOT NULL,
  `emp_number` varchar(255) DEFAULT NULL,
  `emp_name` varchar(255) DEFAULT NULL,
  `emp_mobile` int(10) DEFAULT NULL,
  `emp_division` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`emp_id`, `emp_number`, `emp_name`, `emp_mobile`, `emp_division`, `active`) VALUES
(1, 'E87954', 'Kusum Chandralatha', 717575857, 'IT', 1),
(2, 'E43256', 'Lal Kulathunga', 112857958, 'ADMIN', 0),
(3, 'E37456', 'Mihiri Lokuge', 777682837, 'HR', 1),
(4, 'E37464', 'Roshan Kumara', 718585804, 'ADMIN', 1),
(5, 'E43434', 'Ashan kalusrachi', 718575986, 'HR', 1),
(29, 'E434347', 'Nayani', 783932959, 'IT', 1),
(30, 'E434377', 'Rani', 783932959, 'IT', 1),
(37, 'E434345', 'Niouni', 783932959, 'IT', 0),
(36, 'E434343', 'Ruwani', 783932978, 'IT', 1),
(34, 'E434346', 'Gayani', 783932959, 'IT', 0),
(25, 'E434342', 'Suchini', 783932958, 'IT', 1),
(26, 'E434344', 'Susini', 783932959, 'IT', 1),
(35, 'E434341', 'Ruchini', 783932959, 'IT', 1),
(31, 'E434348', 'Kusuma', 783932959, 'IT', 1),
(32, 'E434358', 'Ravi', 783932959, 'IT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_type` tinyint(1) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `u_email` varchar(100) DEFAULT NULL,
  `u_frist_name` varchar(100) DEFAULT NULL,
  `u_last_name` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created_on` datetime DEFAULT NULL,
  `edited_on` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_type`, `username`, `password`, `u_email`, `u_frist_name`, `u_last_name`, `active`, `created_on`, `edited_on`, `deleted`) VALUES
(1, 1, 'admin_1', '$2y$12$77hD7pfs2.N4AOY72NnPF..cMXF3XdfVo6nZg54HGXrElS1ZMwbDK', 'admin_1@gmail.com', 'f_name_admin_1', 'l_name_admin_1', 1, '2018-01-28 11:22:35', NULL, 0),
(2, 1, 'admin_2', '$2y$12$77hD7pfs2.N4AOY72NnPF..cMXF3XdfVo6nZg54HGXrElS1ZMwbDK', 'admin_2@gmail.com', 'f_name_admin_2', 'l_name_admin_2', 1, '2018-01-28 11:24:07', NULL, 0),
(3, 2, 'user_1', '$2y$12$77hD7pfs2.N4AOY72NnPF..cMXF3XdfVo6nZg54HGXrElS1ZMwbDK', 'user_1@gmail.com', 'f_name_user_1', 'l_name_user_1', 1, '2018-01-28 11:27:32', NULL, 0),
(4, 2, 'user_2', '$2y$12$77hD7pfs2.N4AOY72NnPF..cMXF3XdfVo6nZg54HGXrElS1ZMwbDK', 'user_2@gmail.com', 'f_name_user_2', 'l_name_user_1', 1, '2018-01-28 11:28:47', NULL, 0),
(5, 2, 'user_3', '$2y$12$77hD7pfs2.N4AOY72NnPF..cMXF3XdfVo6nZg54HGXrElS1ZMwbDK', 'user_3@gmail.com', 'f_name_user_3', 'l_name_user_3', 1, '2018-01-28 11:29:30', NULL, 0),
(6, 2, 'user_4', '$2y$12$77hD7pfs2.N4AOY72NnPF..cMXF3XdfVo6nZg54HGXrElS1ZMwbDK', 'user_4@gmail.com', 'f_name_user_4', 'l_name_user_4', 0, '2018-01-28 11:30:20', NULL, 1),
(7, 3, 'manager_1', '$2y$12$77hD7pfs2.N4AOY72NnPF..cMXF3XdfVo6nZg54HGXrElS1ZMwbDK', 'manager_1@gmail.com', 'f_name_manager_1', 'l_name_manager_1', 1, '2018-01-28 11:32:45', NULL, 0),
(8, 4, 'accountant_1', '$2y$12$77hD7pfs2.N4AOY72NnPF..cMXF3XdfVo6nZg54HGXrElS1ZMwbDK', 'accountant_1@gmail.com', 'f_name_accountant_1', 'l_name_accountant_1', 1, '2018-01-28 11:32:54', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_type`
--

CREATE TABLE `tbl_user_type` (
  `ut_id` int(11) NOT NULL,
  `user_type` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_type`
--

INSERT INTO `tbl_user_type` (`ut_id`, `user_type`, `active`) VALUES
(1, 'admin', 1),
(2, 'user', 1),
(3, 'manager', 1),
(4, 'accountant', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
(1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_type`
--
ALTER TABLE `tbl_user_type`
  ADD PRIMARY KEY (`ut_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_user_type`
--
ALTER TABLE `tbl_user_type`
  MODIFY `ut_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
