<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tbl_employee';

    protected $fillable = [
        'emp_number', 'emp_name', 'emp_mobile', 'emp_division', 'active'
    ];


    public static function insertData($data){

        $value=DB::table('tbl_employee')->where('emp_number', $data['emp_number'])->get();
        if($value->count() == 0){
           DB::table('tbl_employee')->insert($data);
        }
     }
  

}
