<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    protected $table = 'tbl_employee';

    protected $fillable = [
        'emp_number', 'emp_name', 'emp_mobile', 'emp_mobile', 'emp_division', 'active'
    ];
    
   
}
