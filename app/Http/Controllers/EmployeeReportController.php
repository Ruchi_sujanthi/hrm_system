<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plans;
use Validator;
use App\User;
use Auth;
use App\Helpers\APIHelper;
use App\Employee;

class EmployeeReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        if($user = Auth::user()) {
            $emplyees = Employee::where(['active' => 1])->paginate(15);
            foreach($emplyees as $emp) {
                $res = [];
                $res['id'] = $emp->emp_id;
                $res['name'] = $emp->emp_name;
                $res['number'] = $emp->emp_number;
                $res['mobile'] = $emp->emp_mobile;
                $res['devision'] = $emp->emp_division;
                array_push($data,$res);
            }
            $result = ['employees'  => $data];
            return view('pages.reports.list',$result);
        } else {
            return view('pages.login');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
