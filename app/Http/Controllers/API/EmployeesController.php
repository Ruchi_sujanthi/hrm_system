<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Helpers\APIHelper;
use App\User;
use App\Employee;

class EmployeesController extends Controller
{
    //get employee list
    Public function GetEmployeeList(Request $request) {
        $data = [];
        $successStatus = 200;
        try{
            $therapists = Employee::where(['active' => 1])->paginate(15);
            foreach($therapists as $therapy) {
                $res[] = $therapy;
            }
            array_push($data,$res);
            
            return response()->json(APIHelper::createAPIResponse($data, $successStatus, 'success'), 
            $successStatus);

        } catch (Exception $e) {
            return response()->json(APIHelper::errorAPIResponse("system_error", 500), 500);
        }
        
    }

}
