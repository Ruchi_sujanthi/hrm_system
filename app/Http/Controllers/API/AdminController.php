<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Employee;
use Auth;
use App\Helpers\APIHelper;

class AdminController extends Controller
{   


    public function logout(Request $request) {
        \Auth::logout();
        return redirect(url('/'));
    }

    public function AdminLogin(Request $request)
    {

        if(Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {
            
            $user_verify = User::where(['username' => $request->input('username')])->first();
           
            $user = Auth::user();
            $success['token'] = $user->createToken('hrm_system')->accessToken;
            $success['name'] = $user->name;
            return redirect(url('/dashboard'));
            
        } else {
        
            return redirect()->back()->with('fail', 'Incorrect username or password, Login failed!'); 
        }  

    }


    public function FileUpload(Request $request) {
        if($request->hasfile('csv_file')){
            if($user = Auth::user()) {
                $header = null;
                $data = array();
                $file = $request->file('csv_file');
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $tempPath = $file->getRealPath();
                $fileSize = $file->getSize();
                $mimeType = $file->getMimeType();

                // Valid File Extensions
                $valid_extension = array("csv");
                // 2MB in Bytes
                $maxFileSize = 2097152; 

                // Check file extension
                if(in_array(strtolower($extension),$valid_extension)){
                    // Check file size
                    if($fileSize <= $maxFileSize){

                        // File upload location
                        $location = 'uploads';

                        // Upload file
                        $file->move($location,$filename);

                        // Import CSV to Database
                        $filepath = public_path($location."/".$filename);

                        // Reading file
                        $file = fopen($filepath,"r");

                        $importData_arr = array();
                        $i = 0;
                        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                            $num = count($filedata );
                            
                            // Skip first row (Remove below comment if you want to skip the first row)
                            /*if($i == 0){
                               $i++;
                               continue; 
                            }*/
                            for ($c=0; $c < $num; $c++) {
                               $importData_arr[$i][] = $filedata [$c];
                            }
                            $i++;
                         }
                         fclose($file);
                         
                         // Insert to MySQL database
                         foreach($importData_arr as $importData){
                            
                            $insertData = array(
                                "emp_number"=>utf8_encode($importData[1]),
                                "emp_name"=>utf8_encode($importData[0]),
                                "emp_mobile"=>utf8_encode($importData[2]),
                                "emp_division"=>utf8_encode($importData[3]),
                                "active"=>$importData[4]);
                                Employee::insertData($insertData);
               
                         }

                         return redirect()->back()->with('message', 'Successfully file uploaded.');

                    }else {
                        return redirect()->back()->with('fail', 'File too large. File must be less than 2MB.');
                        
                    }

                } else {
                    return redirect()->back()->with('fail', 'csv file is required');
                }
                                
            } else {
                return redirect()->back()->with('fail', 'Please loggin to the app');

            }
            
        } else {
            return redirect()->back()->with('fail', 'csv file is required');
        }
    }

    
}