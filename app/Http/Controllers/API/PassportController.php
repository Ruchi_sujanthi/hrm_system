<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use App\Image;
use App\Mails;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Socialite;
use App\Jobs\SendReminderEmail;
use App\Helpers\APIHelper;

class PassportController extends Controller
{

    public function register(Request $request)
    {
        try{
            $successStatus = 200;
        
            if($request['user_role'] == "client") {
                $request['user_role'] = 1;
            } else if ($request['user_role'] == "counsellor") {
                $request['user_role'] = 2;
            } else {
                $request['user_role'] = 3;
            }

            if($request['user_role'] !== 3) {
                //validation
                $validator = Validator::make($request->all(), [
                    'user_role' => 'required|integer',
                    'name' => 'sometimes|nullable|string|max:255',
                    'email' => 'required|email|max:255|unique:users,email',
                    'nickname' => 'sometimes|nullable|string|max:255|unique:users',
                    'password' => 'required',
                    'mobile' => 'required|string',
                    'birthday' => 'required|date',
                    'gender' => 'required|string',
                    'address' => 'required|string',
                    'avatar' => 'sometimes|nullable|image|mimes:jpg,png,jpeg',
                    'anonymous' => 'required|boolean',
                    
                ]);

                if ($validator->fails()) {
                    return response()->json([APIHelper::errorAPIResponse($validator->errors(), 400)],400);
                }
    
            }
        
            
            //success
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] = $user->createToken('hrm_system')->accessToken;
            $success['name'] = $user->name;
            $success['nickname'] = $user->nickname;
            $success['email'] = $user->email;

            //upload avatar to google clouds
            if(!empty($request['avatar'])) {
                $image_upload = Image::avatarUpload($request,$user->id);
                if ($image_upload['success']) {
                    $user->avatar = $image_upload['data'];
                    $user->save();
                } 
            }

            // //upload avatar to google clouds
            // if(iisset($request['certificates']) && !empty($request['certificates'])) {
            //     $certificates = Image::certificateUpload($request,$user->id);
            //     if ($certificates['success']) {
            //         $user->$certificates = $certificates['data'];
            //         $user->save();
            //     } 
            // }

            // send verification code
            $display_name = $user->name;
            if($request['anonymous'] == 1){
                $display_name = $user->nickname;
            } 

            $verify_code = Mails::sendVerifcation($display_name, $user->email);
            if($verify_code) {
                $user->code = $verify_code['code'];
                $user->code_sent_at = $verify_code['sent_time'];
                $user->save();
            }
            
            return response()->json(APIHelper::createAPIResponse($success, $successStatus, 'success'), 
            $successStatus);

        } catch (Exception $e) {
            return response()->json([APIHelper::errorAPIResponse("system_error", 500)], 500);
        }
        
    }

    public function login(Request $request)
    {
        
        $successStatus = 200;
        //validation
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json([APIHelper::errorAPIResponse($validator->errors(), 400)],400);
        }
        
        if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            
            $user_verify = User::where(['email' => $request->input('email')])->first();
            
            if($user_verify['verified'] == 1) {

                $user = Auth::user();
                $success['token'] = $user->createToken('hrm_system')->accessToken;
                $success['name'] = $user->name;
                
                return response()->json(APIHelper::createAPIResponse($success, $successStatus, 'success'), 
                $successStatus);

            } else {
                
                return response()->json([APIHelper::createAPIResponse("please verifiy your account", 400, 'success')], 
                400);
     
            }
            
        } else {
            return response()->json([APIHelper::errorAPIResponse("Unauthorized user", 401)], 401);
        }

       

    }

    public function verifyEmail(Request $request)
    {

        $successStatus = 200;

        //verify user email
        $user = User::where(['email' => $request->input('email')])->first();
        if($user['code'] === $request->input('code')) {
            $success['msg'] = "email verification success";
            $user->verified = true;
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->save();

            // send notification for counselors
            $client_profile = "https://online-theropy-member.arimac.digital/";
            $this->dispatch(new SendReminderEmail($client_profile));

            return response()->json(APIHelper::createAPIResponse($success, $successStatus, 'success'), 
            $successStatus);
        }

        return response()->json([APIHelper::errorAPIResponse("Verification failed", 400)], 400);
        
    }


    public function resendEmail(Request $request)
    {

        $successStatus = 200;

        //verify user email
        $user = User::where(['email' => $request->input('email')])->first();
        if($user) {
            // send verification code
            $display_name = $user->name;
            if($request['anonymous'] == 1){
                $display_name = $user->nickname;
            } 

            $verify_code = Mails::sendVerifcation($display_name, $user->email);
            if($verify_code) {
                $user->code = $verify_code['code'];
                $user->code_sent_at = $verify_code['sent_time'];
                $user->save();
            }
            
            return response()->json(APIHelper::createAPIResponse($success, $successStatus, 'success'), 
            $successStatus);
        }

        return response()->json([APIHelper::errorAPIResponse("Verification failed", 400)], 400);
        
    }

    public function socialLoginFb()
    {
        $provider = Socialite::driver('facebook');
        return $provider->fields([
            'first_name', 'last_name', 'email', 'gender', 'birthday' , 'location' 
        ])->scopes([
            'email', 'public_profile'
        ])->redirect();
    }


    public function handleProviderCallbackFb()
    {
        $successStatus = 200;
        $facebook_user = Socialite::driver('facebook')->fields([
            'first_name', 'last_name', 'email', 'gender', 'birthday' , 'location' 
        ])->user();
        $user = User::where(['email' => $facebook_user->getEmail()])->first();

        if($user) {

            Auth::login($user);
            $success['token'] = $user->createToken('hrm_system')->accessToken;
            $success['name'] = $user->name;
            $success['redirect_page'] = "login_success";

            return response()->json(APIHelper::createAPIResponse($success, $successStatus, 'success'), 
            $successStatus);

        } else {
            
            $success['profile_id'] = $facebook_user->getId();
            $success['name'] = $facebook_user->user['first_name'];
            $success['email'] = $facebook_user->getEmail();
            $success['avatar'] = $facebook_user->avatar_original;
            $success['media'] = "facebook";
            $success['redirect_page'] = "registration_page";

            return response()->json(APIHelper::createAPIResponse($success, $successStatus, 'success'), 
            $successStatus);
       
        }
    }


    public function socialLoginGoogle()
    {
        return Socialite::driver('google')->redirect();
    }


    public function handleProviderCallbackGoogle()
    {
        $successStatus = 200;
        $google_user = Socialite::driver('google')->user();
        $user = User::where(['email' => $google_user->getEmail()])->first();

        if($user) {

            Auth::login($user);
            $success['token'] = $user->createToken('hrm_system')->accessToken;
            $success['name'] = $user->name;
            $success['redirect_page'] = "login_success";

            return response()->json(APIHelper::createAPIResponse($success, $successStatus, 'success'), 
            $successStatus);

        } else {
            
            $success['profile_id'] = $google_user->getId();
            $success['name'] = $google_user->user['name'];
            $success['email'] = $google_user->getEmail();
            $success['avatar'] = $google_user->avatar_original;
            $success['media'] = "google";
            $success['redirect_page'] = "registration_page";

            return response()->json(APIHelper::createAPIResponse($success, $successStatus, 'success'), 
            $successStatus);
       
        }
    }

    public function getProfile()
    {

        $successStatus = 200;
        $user_data = Auth::user();
        
        if($user_data['user_role'] == 1) {
            $user_data['user_role'] = "client";
        } else if ($user_data['user_role'] == 2) {
            $user_data['user_role'] = "counsellor";
        } else {
            $user_data['user_role'] = "admin";
        }
        if($user_data['avatar'] != null) {
            $user_data['avatar'] = Image::viewImage($user_data['avatar']);
        }
        
        return response()->json(APIHelper::createAPIResponse($user_data, $successStatus, 'success'), 
            $successStatus);

    }

    public function logout(Request $request) {
        $successStatus = 200;
        $token = $request->user()->token();
        $token->revoke();
    
        $response = 'You have been succesfully logged out!';
        return response()->json(APIHelper::createAPIResponse($response, $successStatus, 'success'), 
            $successStatus);
    
    }
}
