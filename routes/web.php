<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.login');
})->name("admin.login");



// protected routes
Route::group(['middleware' => 'auth'], function() {
    
    /* Dashbaord routes */
    Route::resource('/dashboard', 'DashboardController');
    /* Dashbaord routes */

    /* Reports route */
    Route::resource('/reports', 'EmployeeReportController');
    /* Reports route */

});



