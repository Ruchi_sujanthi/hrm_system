@extends('layouts.default')
@section('content')
    <div class="row no-gutters payment-list">
        <div class="col-md-10 offset-md-1">
            <div class="d-flex flex-row align-items-center page-head-wrap">
                <span class="main-title">Employee Report</span>
            </div>
            @if(session()->has('message'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('message') }}
            </div>
            @elseif(session()->has('fail'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('fail') }}
            </div>
            @endif
            @if(isset($employees))
                <div class="table-content bg-white">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col" class="border-0">Name</th>
                            <th scope="col" class="border-0">Number</th>
                            <th scope="col" class="border-0">Devision</th>
                            <th scope="col" class="border-0">Mobile</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($employees as $emp)
                        <tr>
                            <th scope="row">{{ $emp['name'] }}</th>
                            <th scope="row">{{ $emp['number'] }}</th>
                            <th scope="row">{{ $emp['devision'] }}</th>
                            <th scope="row">{{ $emp['mobile'] }}</th>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>

@endsection


{{--
<div class="links">
    <a href="https://github.com/laravel/laravel">GitHub</a>
    <a href="api/socialLoginFb">Facebook</a>
    <a href="api/socialLoginGoogle">Google</a>

</div>
--}}
