@extends('layouts.default')
@section('content')
<form method="post" action="{{url('api/ResetPassowrd')}}" enctype="multipart/form-data">
    <div class="row no-gutters payment-list create" id="user-edit">
        <div class="col-md-12">
            <div class="page-head-wrap">
                <span class="main-title">Reset password</span>
            </div>
        </div>
        @if(session()->has('message'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('message') }}
            </div>
            @elseif(session()->has('fail'))
            <div class="alert alert-danger" role="alert">
                {{ session()->get('fail') }}
            </div>
        @endif
        <div class="col-md-12 bg-white table-content">
            <div class="col-md-6 offset-md-3">
                <input type="hidden" name="id" value="{{ $user['id'] }}">
                <div class="form-group">
                        <label for="">New Password</label>
                        <input name="password" type="password" class="form-control main-input" placeholder="Enter new password">
                </div>
                <div class="form-group">
                    <label for="">Confirm Password</label>
                    <input name="password_confirmation" type="password"  class="form-control main-input" placeholder="Confrim password">
                </div>
                <div class="cre-btn text-right mt-4">
                    <a href="{{ url()->previous() }}" class="main-btn">Back</a>
                    <button type="submit" class="main-btn">Reset</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        let userEdit = new Vue({
            el: '#user-create',
            data() {
                return {
                    selected: 'role_1'
                };

            }
        })
    </script>
</form>
@endsection




