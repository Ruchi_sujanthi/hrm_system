@extends('includes.head')

<form method="post" action="{{url('api/AdminLogin')}}" enctype="multipart/form-data">
    <div class="row no-gutters">
            <div class="col-md-4 offset-md-4">
                        @if(session()->has('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('message') }}
                        </div>
                        @elseif(session()->has('fail'))
                        <div class="alert alert-danger" role="alert">
                            {{ session()->get('fail') }}
                        </div>
                        @endif
                <div class="row login-wrap">
                    <div class="login bg-white">
                        <h4>QUALITAPPS</h4>
                        <p>Human Resuorce Management System</p>

                        <div class="form-group">
                            <input name="username" type="text" class="form-control  main-input" placeholder="username">
                        </div>
                        <div class="form-group">
                            <input name="password" type="password" class="form-control  main-input" placeholder="password">
                        </div>

                        <div class="btn-wrap text-center">
                            <button type="submit" class="main-btn">Login</button>
                        </div>
                        
                    </div>  
                </div>
            </div>
        </div>
    </form>

