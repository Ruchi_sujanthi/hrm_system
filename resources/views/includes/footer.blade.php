<template>
    <div>
        <div class="row footer-wrap no-gutters align-items-center">

            <div class="col-md-4 col-12 start">
                Arimac Digital @ 2019 All Rights Reserved
            </div>
            <div class="col-md-4 col-12 text-center middle">Privacy Policy | Legal | Terms of Sale</div>
            <div class="col-md-4 col-12 d-flex justify-content-end end">
                <div class="follow-us align-self-center mr-1">Follow us on</div>
                <div class="icon mx-2"><a href=""><span class="helper"></span><i class="fab fa-facebook-f"></i></a></div>
                <div class="icon mx-2"><a href=""><span class="helper"></span><i class="fab fa-youtube"></i></a></div>
                <div class="icon mx-2"><a href=""><span class="helper"></span><i class="fab fa-instagram"></i></a></div>
                <div class="icon mx-2"><a href=""><span class="helper"></span><i class="fab fa-linkedin-in"></i></a></div>
            </div>
        </div>
    </div>
</template>

<script>
    export default {
        name: "Footer"
    }
</script>

<style scoped>

</style>
