<div class="nav-wrap">
    <nav class="navbar navbar-expand-lg navbar-light bg-light cust-nav">
        <a class="navbar-brand" href="">QUALITAPPS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form class="form-inline my-2 my-lg-0 ml-auto">
                <a href="" >
                    <span>{{Auth::user()->username}}</span>
                </a><br></br>
                <a href="{{url('api/logout')}}" class="profile-log-wrap">
                   <img src="{{asset("images/dp.png")}}"
                        class="img-fluid" alt="logo"/>
                   <span>LOGOUT</span>
               </a>
            </form>
        </div>
    </nav>
</div>
